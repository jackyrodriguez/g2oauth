<?php

class oauthClient {

	public function getAccessToken($access_data, $short_code) {

		$url = 'http://developer.globelabs.com.ph/oauth/access_token?';
		$fields = array(
					'app_id' => urlencode($access_data['app_id']),
					'redirect_uri' => urlencode($access_data['redirect_uri']),
					'app_secret' => urlencode($access_data['app_secret']),
					'code' => urlencode($short_code)
				);

		return $this->getData($fields, 'POST', $url); 
	}	

	public function transactionCharge($send_charge_params) {

		$url = 'http://devapi.globelabs.com.ph/payment/v1/transactions/amount/?';
		$fields = array(
				'app_id' => urlencode($send_charge_params['app_id']),
				'access_token' => urlencode($send_charge_params['access_token']),
				'amount' => urldecode('0'),
				'endUserId' => urlencode($send_charge_params['address']),
				'referenceCode' => urldecode($send_charge_params['ref_code']),
				'rate' => urlencode('referenceCode')
				);

		return $this->getData($fields, 'POST', $url );
	}

	public function sendMessage($send_message_params, $short_code) {

		$url = 'http://devapi.globelabs.com.ph/smsmessaging/v1/outbound/'.$short_code.'/requests?';
		$fields = array(
					'app_id' => urlencode($send_message_params['app_id']),
					'access_token' => urlencode($send_message_params['access_token']),
					'address' => urlencode($send_message_params['subscriber_number']),
					'message' => urlencode($send_message_params['message'])
				);

		return $this->getData($fields, 'POST', $url );
	}

	public function getData($fields = array(), $method, $url) {

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);

		if ($method == 'POST') {
			foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
			rtrim($fields_string, '&');

			curl_setopt($ch,CURLOPT_POST, count($fields));
			curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);

		} else {

			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		}	
			
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			curl_close($ch);
			
			return json_decode($result, true);

	} // END getData


}// End class
